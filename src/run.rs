mod builder;
mod status_check;
mod statuses;

use std::collections::VecDeque;
use std::fmt::Display;
use std::path::Path;
use std::process::Command;
use std::time::{Duration, Instant};

use anyhow::{bail, Result as AhResult};
use duct::{Expression, Handle};
use wax::Glob;
use yansi::Paint;

use builder::Builder;
use statuses::Statuses;

use crate::cli::RunArgs;
use crate::paths;
use crate::paths::Paths;
use crate::table::{Slot, Table};

#[derive(Debug)]
struct Build {
    package: String,
    proc_handle: Handle,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum XbpsStatus {
    Good,
    Bad,
    MissingDependency,
    MissingSetId,
    BadChecksum,
    BadFetch,
    UnsupportedArch,
    NoCross,
    Broken,
}

impl Display for XbpsStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Good => write!(f, "{}", "Success".green().bold()),
            Self::Bad => write!(f, "{}", "Fail".red().bold()),
            Self::MissingDependency => write!(f, "{}", "Missing Dependency".yellow().bold()),
            Self::MissingSetId => write!(f, "{}", "Missing setuid/setgid".yellow().bold()),
            Self::BadChecksum => write!(f, "{}", "Bad Checksum".red().bold()),
            Self::BadFetch => write!(f, "{}", "Fetch Failed".red().bold()),
            Self::UnsupportedArch => {
                write!(f, "{}", "Selected Arch Unsupported".yellow().bold())
            }
            Self::NoCross => write!(f, "{}", "Cannot be cross-built".yellow().bold()),
            Self::Broken => write!(f, "{}", "Marked Broken".yellow().bold()),
        }
    }
}

#[derive(Debug)]
struct Params {
    procs: u64,
    jobs: Option<u64>,
    poll_period: Duration,
    masterdir_arch: Option<String>,
    cross_arch: Option<String>,
    xbps_src_flags: Vec<String>,
}

#[derive(Debug)]
pub struct Run {
    params: Params,
    paths: Paths,
    builds: Table<Build>,
    statuses: Statuses,
}

fn prepare_params(args: &RunArgs) -> Params {
    let poll_secs = args.poll_period.unwrap_or(1);

    Params {
        procs: args.procs.unwrap_or(1),
        jobs: args.jobs,
        poll_period: Duration::from_secs(poll_secs),
        masterdir_arch: args.masterdir_arch.clone(),
        cross_arch: args.cross_arch.clone(),
        xbps_src_flags: args.xbps_src_flags.clone(),
    }
}

fn n_digits(value: usize) -> usize {
    let mut value = value;
    let mut result = 1;
    while value >= 10 {
        result += 1;
        value /= 10;
    }

    result
}

pub fn clean() -> AhResult<()> {
    let glob = Glob::new("{report.toml,logs,masterdir_*}")?;
    let state_dir = paths::state_path()?;

    for entry in glob.walk(state_dir) {
        let entry = entry?;
        let path = entry.path();

        eprintln!("Removing {}", path.display());

        let metadata = entry.metadata()?;
        if metadata.is_file() {
            std::fs::remove_file(path)?;
        } else {
            std::fs::remove_dir_all(path)?;
        }
    }

    Ok(())
}

impl Build {
    pub fn determine_status(&self) -> AhResult<XbpsStatus> {
        let status = self.proc_handle.wait()?.status;

        if status.success() {
            return Ok(XbpsStatus::Good);
        }

        let log_dir = paths::logs_path()?;
        let pkg_log_dir = log_dir.join(&self.package);
        let pkg_log_file = pkg_log_dir.join("log.txt");

        let content = std::fs::read_to_string(&pkg_log_file)?;
        let checked_lines = content.lines().rev().take(3);

        for line in checked_lines {
            if status_check::is_bad_checksum(line) {
                return Ok(XbpsStatus::BadChecksum);
            }

            if status_check::is_bad_fetch(line) {
                return Ok(XbpsStatus::BadFetch);
            }

            if status_check::is_missing_dependency(line) {
                return Ok(XbpsStatus::MissingDependency);
            }

            if status_check::is_missing_set_id(line) {
                return Ok(XbpsStatus::MissingSetId);
            }

            if status_check::is_unsupported_arch(line) {
                return Ok(XbpsStatus::UnsupportedArch);
            }

            if status_check::is_nocross(line) {
                return Ok(XbpsStatus::NoCross);
            }

            if status_check::is_broken(line) {
                return Ok(XbpsStatus::Broken);
            }
        }

        Ok(XbpsStatus::Bad)
    }
}

impl Run {
    pub fn new(args: &RunArgs, paths: Paths) -> Self {
        let params = prepare_params(args);
        let procs = params.procs;
        Self {
            params,
            paths,
            builds: Table::new(procs as _),
            statuses: Statuses::new(),
        }
    }

    pub fn statuses(&self) -> &Statuses {
        &self.statuses
    }

    fn poll_finished(&mut self) -> AhResult<Option<(Slot, Build)>> {
        std::thread::sleep(self.params.poll_period);

        self.builds.try_release_slot_if(|build| {
            build
                .proc_handle
                .try_wait()
                .map(|maybe_status| maybe_status.is_some())
                .map_err(Into::into)
        })
    }

    fn bootstrap_command(&self, masterdir: &Path) -> Command {
        let xbps_src = self.paths.xbps_src();
        let mut result = Command::new(xbps_src);

        result.args(["binary-bootstrap", "-m"]);
        result.arg(masterdir);
        if let Some(arch) = &self.params.masterdir_arch {
            result.arg("-A");
            result.arg(arch);
        }

        result
    }

    fn build_command(&self, masterdir: &Path, package: &str) -> Expression {
        let xbps_src = self.paths.xbps_src();
        let mut result = Builder::new(xbps_src);

        if let Some(jobs) = self.params.jobs {
            result.arg("-j").arg(jobs.to_string());
        }

        result.arg("-m").arg(masterdir);

        result.args(&self.params.xbps_src_flags);

        if let Some(cross_arch) = &self.params.cross_arch {
            result.arg("-a").arg(cross_arch);
        }

        result.arg("pkg").arg(package);

        result.build()
    }

    fn finish_builds(&mut self) -> AhResult<()> {
        while !self.builds.is_empty() {
            let Some((_, build)) = self.poll_finished()? else {
                continue;
            };

            let status = build.determine_status()?;

            eprintln!(
                "Finished building {} with status {}",
                build.package.bold(),
                status
            );

            self.statuses.add(status, build.package);
        }

        Ok(())
    }

    fn summarize(&self, duration: Duration, total: usize) {
        let elapsed = duration.as_secs();
        let secs = elapsed % 60;
        let mins = (elapsed / 60) % 60;
        let hours = elapsed / 3600;
        eprintln!(
            "=> Built {} packages in {:02}:{:02}:{:02}",
            total, hours, mins, secs
        );

        let width = n_digits(total);
        for (status, pkgs) in &self.statuses {
            if !pkgs.is_empty() {
                eprintln!("    {:>width$} {}", pkgs.len(), status, width = width);
            }
        }
    }

    pub fn boostrap_masterdirs(&self) -> AhResult<()> {
        let state_dir = paths::state_path()?;

        for n in 0..self.params.procs {
            let dir_name = format!("masterdir_{:03}", n);
            eprintln!("Bootstrapping {}", dir_name.bold());

            let path = state_dir.join(dir_name);
            std::fs::create_dir_all(&path)?;

            let mut bootstrap_process = self.bootstrap_command(&path);
            let status = bootstrap_process.status()?;

            if !status.success() {
                bail!("Failed to bootstrap masterdir: {}", path.display());
            }
        }

        Ok(())
    }

    pub fn execute(&mut self, packages: &[String]) -> AhResult<()> {
        let state_dir = paths::state_path()?;
        let log_dir = paths::logs_path()?;

        let mut packages: VecDeque<_> = packages.iter().cloned().collect();
        let mut progress = 0;
        let total = packages.len();
        let digits = n_digits(total);
        let start = Instant::now();

        loop {
            let free_slot = match self.builds.free_slot() {
                Some(slot) => slot,
                None => {
                    let Some((slot, build)) = self.poll_finished()? else {
                        continue;
                    };

                    let status = build.determine_status()?;

                    eprintln!(
                        "Finished building {} with status {}",
                        build.package.bold(),
                        status
                    );

                    self.statuses.add(status, build.package);

                    slot
                }
            };

            let Some(pkg) = packages.pop_front() else {
                break;
            };

            let slot_number = free_slot.number();
            let dir_name = format!("masterdir_{:03}", slot_number);
            let masterdir = state_dir.join(&dir_name);

            progress += 1;
            let counter = format!("[{:>width$}/{}]", progress, total, width = digits);
            eprintln!(
                "=> {} Building {} in {}",
                counter,
                pkg.as_str().bold(),
                dir_name.bold()
            );

            let pkg_log_dir = log_dir.join(&pkg);
            let pkg_log_file = pkg_log_dir.join("log.txt");

            std::fs::create_dir_all(&pkg_log_dir)?;

            let pkg_process = self.build_command(&masterdir, &pkg);
            let proc_handle = pkg_process
                .stderr_to_stdout()
                .stdout_path(&pkg_log_file)
                .unchecked()
                .start()?;

            let build = Build {
                package: pkg,
                proc_handle,
            };

            self.builds.set(free_slot, build);
        }

        self.finish_builds()?;

        let duration = start.elapsed();
        self.summarize(duration, total);

        Ok(())
    }
}
