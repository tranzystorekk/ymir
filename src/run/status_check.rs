use once_cell::sync::Lazy;
use regex::Regex;

static MISSING_DEP_PATTERN: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"ERROR: dep .* not found: -1 passed: instructed not to build").unwrap()
});
static BAD_CHECKSUM_PATTERN: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"ERROR: .*: couldn't verify distfiles, .*").unwrap());
static BAD_FETCH_PATTERN: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"ERROR: .*: failed to fetch .*").unwrap());
static MISSING_SET_ID_PATTERN: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"=> ERROR: set[gu]id files not explicitly allowed, please list them in \$set[gu]id")
        .unwrap()
});
static UNSUPPORTED_ARCH_PATTERN: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"=> ERROR: .*: this package cannot be built for .*").unwrap());
static NOCROSS_PATTERN: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"=> ERROR: .*: cannot be cross compiled\.\.\.").unwrap());
static BROKEN_PATTERN: Lazy<Regex> = Lazy::new(|| {
    Regex::new(r"=> ERROR: .*: cannot be built, it's currently broken; see the build log:").unwrap()
});

pub fn is_missing_dependency(line: &str) -> bool {
    MISSING_DEP_PATTERN.is_match(line)
}

pub fn is_bad_checksum(line: &str) -> bool {
    BAD_CHECKSUM_PATTERN.is_match(line)
}

pub fn is_bad_fetch(line: &str) -> bool {
    BAD_FETCH_PATTERN.is_match(line)
}

pub fn is_missing_set_id(line: &str) -> bool {
    MISSING_SET_ID_PATTERN.is_match(line)
}

pub fn is_unsupported_arch(line: &str) -> bool {
    UNSUPPORTED_ARCH_PATTERN.is_match(line)
}

pub fn is_nocross(line: &str) -> bool {
    NOCROSS_PATTERN.is_match(line)
}

pub fn is_broken(line: &str) -> bool {
    BROKEN_PATTERN.is_match(line)
}
