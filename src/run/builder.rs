use duct::Expression;

use std::ffi::{OsStr, OsString};

/// Adds [`std::process::Command`] semantics to the [`duct::Expression`] type
pub struct Builder {
    cmd: OsString,
    args: Vec<OsString>,
}

impl Builder {
    pub fn new<S: AsRef<OsStr>>(cmd: S) -> Self {
        Self {
            cmd: cmd.as_ref().to_os_string(),
            args: vec![],
        }
    }

    pub fn arg<S: AsRef<OsStr>>(&mut self, arg: S) -> &mut Self {
        self.args.push(arg.as_ref().to_os_string());
        self
    }

    pub fn args<I: IntoIterator<Item = S>, S: AsRef<OsStr>>(&mut self, args: I) -> &mut Self {
        self.args
            .extend(args.into_iter().map(|arg| arg.as_ref().to_os_string()));
        self
    }

    pub fn build(&self) -> Expression {
        duct::cmd(&self.cmd, &self.args)
    }
}
