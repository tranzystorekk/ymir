use anyhow::Result as AhResult;

#[derive(Debug)]
pub struct Table<T> {
    slots: Vec<Option<T>>,
}

#[derive(Debug)]
pub struct Slot {
    number: usize,
}

impl<T> Table<T> {
    pub fn new(n: usize) -> Self {
        let slots = std::iter::repeat_with(|| None).take(n).collect();

        Self { slots }
    }

    pub fn is_empty(&self) -> bool {
        self.slots.iter().all(|slot| slot.is_none())
    }

    pub fn free_slot(&mut self) -> Option<Slot> {
        self.slots
            .iter_mut()
            .enumerate()
            .find(|(_, slot)| slot.is_none())
            .map(|(number, _)| Slot { number })
    }

    pub fn try_release_slot_if<F: FnMut(&mut T) -> AhResult<bool>>(
        &mut self,
        mut try_pred: F,
    ) -> AhResult<Option<(Slot, T)>> {
        for (n, slot) in self.slots.iter_mut().enumerate() {
            if let Some(v) = slot {
                if !try_pred(v)? {
                    continue;
                }

                let result = slot.take().map(|v| (Slot { number: n }, v));

                return Ok(result);
            }
        }

        Ok(None)
    }

    pub fn set(&mut self, slot: Slot, v: T) {
        self.slots[slot.number] = Some(v);
    }
}

impl Slot {
    pub fn number(&self) -> usize {
        self.number
    }
}
